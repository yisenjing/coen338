a=imread('PeppersRGB.tif');
figure(1);imshow(a);
title('Original Image');

[ca1,ch1,cv1,cd1] = dwt2(a,'haar');

a1=upcoef2('a',ca1,'haar',1);
h1=upcoef2('h',ch1,'haar',1);
v1=upcoef2('v',cv1,'haar',1);
d1=upcoef2('d',cd1,'haar',1);
 
figure(2);
subplot(2,2,1);imshow(a1/255);title('a1');
subplot(2,2,2);imshow(h1);title('h1');
subplot(2,2,3);imshow(v1);title('v1');
subplot(2,2,4);imshow(d1);title('d1');

m=abs(a1).^2;
n=abs(a1.^2+h1.^2+v1.^2+d1.^2);
fprintf('\n The energy percent is %0.4f \n', sum(sum(m,1),2) ./ sum(sum(n,1),2));


a1=(a1)/255; a1=im2uint8(a1); figure(3);
subplot(1,2,1);imshow(a);title('Original Image');
figure(4);imshow(a1);title('The image after Compression');

[peaksnr, snr] = psnr(a1, a);
 fprintf('\n The Peak-SNR value is %0.4f', peaksnr);
 fprintf('\n The SNR value is %0.4f \n', snr);
 
 ssimval = ssim(a1,a);
 fprintf('\n The SSIM value is %0.4f \n', ssimval);
 imwrite(a1,'a1.tiff');
 

