%%%%%%  show original image %%%%%%%%
i1 = imread('PeppersRGB.tif');
i1 = im2double(i1);
a = i1(10,10);
ycbcr_i1 = rgb2ycbcr(i1);

%%%%%%% extract YCbCr Channel %%%%%%%%%%

Y = ycbcr_i1(:,:,1);
Cb = ycbcr_i1(:,:,2);
Cr = ycbcr_i1(:,:,3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j=dct2(Y);
figure(1);imshow(log(abs(j)),[]);title('DCT');colormap(jet(64));colorbar;


a = zeros(size(j));
a(1:16, 1:16) = 1;
e1 = (abs(j).*a).^2;
e2 = abs(j).^2;
c1 = sum(sum(e1,1),2);
c2 = sum(sum(e2,1),2);
fprintf('\n The energy percent is %0.4f \n', c1/c2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T=dctmtx(8);
Q = [1 1 1 1 0 0 0 0
     1 1 1 0 0 0 0 0
     1 1 0 0 0 0 0 0
     1 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Y = blockproc(Y,[8 8],fun1);
Q_Y = blockproc(DCT_Y,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Y = blockproc(Q_Y,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Cb = blockproc(Cb,[8 8],fun1);
Q_Cb = blockproc(DCT_Cb,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Cb = blockproc(Q_Cb,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Cr = blockproc(Cr,[8 8],fun1);
Q_Cr = blockproc(DCT_Cr,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Cr = blockproc(Q_Cr,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


i2(:,:,1)=After_Y;i2(:,:,2)=After_Cb;i2(:,:,3)=After_Cr;
rgb_i2 = ycbcr2rgb(i2);

 figure(2);
 subplot(1,2,1),imshow(i1),title('Original Image');
 figure(4);imshow(rgb_i2),title('The Image After Compression');
 
 [peaksnr, snr] = psnr(i1, rgb_i2);
 fprintf('\n The Peak-SNR value is %0.4f', peaksnr);
 fprintf('\n The SNR value is %0.4f \n', snr);
 
 ssimval = ssim(rgb_i2,i1);
 fprintf('\n The SSIM value is %0.4f \n', ssimval);
 imwrite(rgb_i2,'rgb_i2.tiff');
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 
 
