%%%%%%%%%%%%%%%%%%%%%%%%% DWT experiment begin here %%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%first read image %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
a=imread('PeppersRGB.tif');

%%%%%%%%%%%%%%%% do wavelet analyse %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ca1,ch1,cv1,cd1] = dwt2(a,'haar');

a1=upcoef2('a',ca1,'haar',1);
h1=upcoef2('h',ch1,'haar',1);
v1=upcoef2('v',cv1,'haar',1);
d1=upcoef2('d',cd1,'haar',1);


%%%%%%%%%%%%%%% show the image from DWT analyse %%%%%%%%%%%%%%%%%

 
figure(3);
subplot(2,2,1);imshow(a1/255);title('DWT_a1');
subplot(2,2,2);imshow(h1);title('DWT_h1');
subplot(2,2,3);imshow(v1);title('DWT_v1');
subplot(2,2,4);imshow(d1);title('DWT_d1');


%%%%%%%%%%%%%%%%%%%  calculate energy percent %%%%%%%%%%%%%%%%%%%%%%%%%%%%
m=abs(a1).^2;
n=abs(a1.^2+h1.^2+v1.^2+d1.^2);
fprintf('\n The energy percent is %0.4f \n', sum(sum(m,1),2) ./ sum(sum(n,1),2));
a1=(a1)/255; %a1=im2uint8(a1);


%a1 = im2double(a1);
ycbcr_a1 = rgb2ycbcr(a1);

Y = ycbcr_a1(:,:,1);
Cb = ycbcr_a1(:,:,2);
Cr = ycbcr_a1(:,:,3);

figure(10);
subplot(1,3,1),imshow(Y),title('Y');
subplot(1,3,2),imshow(Cb),title('Cb');
subplot(1,3,3),imshow(Cr),title('Cr');

j=dct2(Y);
figure(1);imshow(log(abs(j)),[]);title('DCT_DCT_energy');colormap(jet(256));

%%%%%%%%%%%%%%%%%%%%% calculate energy of the image %%%%%%%%%%
aa = zeros(size(j));
aa(1:16, 1:16) = 1;
e1 = (abs(j).*aa).^2;
e2 = abs(j).^2;
c1 = sum(sum(e1,1),2);
c2 = sum(sum(e2,1),2);
fprintf('\n The energy percent is %0.4f \n', c1/c2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T=dctmtx(8);
Q = [1 1 1 1 1 0 0 0
     1 1 1 1 0 0 0 0
     1 1 1 0 0 0 0 0
     1 1 0 0 0 0 0 0
     1 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0
     0 0 0 0 0 0 0 0];
%%%%%%%%                           %%%%%%%%%%%%%%%
%%%%%%% DCT and quantization steps%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Y = blockproc(Y,[8 8],fun1);
Q_Y = blockproc(DCT_Y,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Y = blockproc(Q_Y,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Cb = blockproc(Cb,[8 8],fun1);
Q_Cb = blockproc(DCT_Cb,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Cb = blockproc(Q_Cb,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fun1 = @(block_struct) T * block_struct.data * T';DCT_Cr = blockproc(Cr,[8 8],fun1);
Q_Cr = blockproc(DCT_Cr,[8 8],@(block_struct) Q .* block_struct.data);
invdct = @(block_struct) T' * block_struct.data * T;After_Cr = blockproc(Q_Cr,[8 8],invdct);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%  generate final image after process %%%%%%%%%%%%%%
a2(:,:,1)=After_Y;a2(:,:,2)=After_Cb;a2(:,:,3)=After_Cr;
rgb_a2 = ycbcr2rgb(a2);

 figure(2);
 imshow(a1),title('Original Image');
 figure(13);
 imshow(rgb_a2),title('DWT_DCT The Image After Compression');

rgb_a2=im2uint8(rgb_a2);
[peaksnr, snr] = psnr(rgb_a2, a);
fprintf('\n The Peak-SNR value by using DWT first then DCT method is %0.4f', peaksnr);
fprintf('\n The SNR value is %0.4f \n', snr);
 
ssimval = ssim(rgb_a2,a);
fprintf('\n The SSIM value is %0.4f \n', ssimval);
 imwrite(rgb_a2,'rgb_a2.tiff');
 
s = dir('rgb_a2.tiff');
s1 = dir('PeppersRGB.tif');
fprintf('\n The File size after DWT-DCT is %0.3f Bytes \n', s.bytes / 1000);
fprintf('\n The File size before DWT-DCT is %0.3f Bytes \n', s1.bytes / 1000);

ycbcr_rgb_a2 = rgb2ycbcr(rgb_a2);
Y = ycbcr_rgb_a2(:,:,1);
j=dct2(Y);
figure(11);imshow(log(abs(j)),[]);title('ycbcr_rgb_a2');colormap(jet(64));
 
 

